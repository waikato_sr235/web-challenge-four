DROP TABLE IF EXISTS challenge4_walks;
DROP TABLE IF EXISTS challenge4_bookings;
DROP TABLE IF EXISTS challenge4_statuses;
DROP TABLE IF EXISTS challenge4_dogs;
DROP TABLE IF EXISTS challenge4_owners;
DROP TABLE IF EXISTS challenge4_walkers;
DROP TABLE IF EXISTS challenge4_suburbs;
DROP TABLE IF EXISTS challenge4_regions;
DROP TABLE IF EXISTS challenge4_breeds;
DROP TABLE IF EXISTS challenge4_sizes;

# id not technically needed, but makes for nice logic to validate whether a dog is appropriately sized for its walker
CREATE TABLE IF NOT EXISTS challenge4_sizes (
    id INT PRIMARY KEY,
    name VARCHAR(10)
);

CREATE TABLE IF NOT EXISTS challenge4_breeds (
    name VARCHAR(25) PRIMARY KEY,
    size INT NOT NULL,
    FOREIGN KEY (size) REFERENCES challenge4_sizes(id)
);

CREATE TABLE IF NOT EXISTS challenge4_regions (
    name VARCHAR(20) PRIMARY KEY
);


CREATE TABLE IF NOT EXISTS challenge4_suburbs (
    postcode VARCHAR(4),
    name VARCHAR(20),
    region VARCHAR(20) NOT null,
    PRIMARY KEY (postcode, name), # Composite primary key needed as there isn't a 1:1 relationship between suburbs and postcodes
    FOREIGN KEY (region) REFERENCES challenge4_regions(name)
);

CREATE TABLE IF NOT EXISTS challenge4_walkers (
    ir_num INT PRIMARY KEY,
    fname VARCHAR(20),
    lname VARCHAR(40) NOT NULL,
    region VARCHAR(20) NOT NULL,
    daily_time_minutes INT NOT NULL,
    max_size int NOT NULL,
    FOREIGN KEY (region) REFERENCES challenge4_regions(name),
    FOREIGN KEY (max_size) REFERENCES challenge4_sizes(id)
);

CREATE TABLE IF NOT EXISTS challenge4_owners (
    id INT PRIMARY KEY AUTO_INCREMENT,
    phone VARCHAR(13),
    email VARCHAR(100),
    fname VARCHAR(20),
    lname VARCHAR(40),
    CONSTRAINT phone_number CHECK (
           phone RLIKE '\\(0[3-9]\\) \\d\\d\\d-\\d\\d\\d\\d'   # valid NZ area codes are 03 or greater
        OR phone RLIKE '02\\d \\d\\d\\d \\d\\d\\d'
        OR phone RLIKE '02\\d \\d\\d\\d \\d\\d\\d\\d'
        OR phone RLIKE '02\\d \\d\\d\\d \\d\\d\\d\\d\\d')       # all NZ cell phone numbers start 02
);

CREATE TABLE IF NOT EXISTS challenge4_dogs (
    registration INT PRIMARY KEY,
    name VARCHAR(40),
    dob DATE,
    breed VARCHAR(25),
    temperament VARCHAR(20),
    equipped BOOL,
    owner INT,
    streetAddress VARCHAR(40),
    region VARCHAR(20),
    FOREIGN KEY (breed) REFERENCES challenge4_breeds(name),
    FOREIGN KEY (owner) REFERENCES challenge4_owners(id) ON DELETE CASCADE,
    FOREIGN KEY (region) REFERENCES challenge4_regions(name)
);

CREATE TABLE IF NOT EXISTS challenge4_statuses (
    name VARCHAR(20) PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS challenge4_bookings (
    id INT PRIMARY KEY AUTO_INCREMENT,
    forDog INT,
    length_minutes INT NOT NULL,
    walkDate DATE NOT NULL,
    bookDate DATE NOT NULL,
    status VARCHAR(20) NOT NULL,
    contactDate DATE,
    FOREIGN KEY (forDog) REFERENCES challenge4_dogs(registration) ON DELETE SET NULL,
    FOREIGN KEY (status) REFERENCES challenge4_statuses(name),
    CHECK (DATEDIFF(walkDate, bookDate) >= 3)
);

CREATE TABLE IF NOT EXISTS challenge4_walks (
    forBooking INT PRIMARY KEY,
    walker INT,
    FOREIGN KEY (walker) REFERENCES challenge4_walkers(ir_num) ON DELETE SET NULL
);

