INSERT INTO sizes VALUES
(1, 'XSmall'),
(2, 'Small'),
(3, 'Medium'),
(4, 'Large'),
(5, 'XLarge');

INSERT INTO breeds VALUES
('Affenpinscher', 1),
('Chinese Crested', 1),
('Russian Tsvetnaya Bolonka', 1),
('Russell Terrier', 2),
('English Toy Spaniel', 2),
('Miniature Bull Terrier', 2),
('Mountain Cur', 3),
('Transylvanian Hound', 3),
('English Springer Spaniel', 3),
('Entlebucher Mountain Dog', 3),
('Estrela Mountain Dog', 4),
('Belgian Malinois', 4),
('Borzoi', 4),
('Broholmer', 5),
('Great Dane', 5),
('Saint Bernard', 5);

INSERT INTO regions VALUES
('Central Wellington'),
('South Coast'),
('Miramar Peninsula'),
('Kelburn Hills'),
('Northern Region');

INSERT INTO suburbs VALUES
('6021', 'Te Aro', 'Central Wellington'),
('6011', 'Mt Cook', 'Central Wellington'),
('6011', 'Mt Victoria', 'Central Wellington'),
('6022', 'Lyall Bay', 'South Coast'),
('6023', 'Island Bay', 'South Coast'),
('6022', 'Miramar', 'Miramar Peninsula'),
('6022', 'Seatoun', 'Miramar Peninsula'),
('6012', 'Kelburn', 'Kelburn Hills'),
('6012', 'Karori', 'Kelburn Hills'),
('5028', 'Tawa', 'Northern Region'),
('6037', 'Johnsonville', 'Northern Region');


INSERT INTO walkers VALUES
(1, 'Sam', 'Darnold', 'South Coast', 360, 5),
(2, 'Luke', 'Falk', 'South Coast', 15, 4),
(3, 'Trevor', 'Siemian', 'South Coast', 120, 3),
(4, 'Sam', 'Darnold', 'Central Wellington', 540, 4),
(5, 'Bryce', 'Petty', 'Central Wellington', 480, 3),
(6, 'Ryan', 'Fitzpatrick', 'Miramar Peninsula', 480, 2),
(7, 'Geno', 'Smith', 'Miramar Peninsula', 270, 1),
(8, 'Mark', 'Sanchez', 'Miramar Peninsula', 270, 2),
(9, 'Brooks', 'Bollinger', 'Kelburn Hills', 30, 3),
(10, 'Brett', 'Favre', 'Kelburn Hills', 480, 1),
(11, 'Kellen', 'Clemens', 'Kelburn Hills', 480, 5),
(12, 'Vinny', 'Testaverde', 'South Coast', 330, 4),
(13, 'Chad', 'Pennington', 'Kelburn Hills', 330, 5);


INSERT INTO owners (phone, email, fname, lname) VALUES
('(03) 321-1234', 'joe@1969.com', 'joe', 'namath'),
('021 987 654', 'winston@1969.com', 'winston', 'hill'),
('021 987 6543', 'matt@1969.com', 'matt', 'snell'),
('021 987 65432', 'don@1969.com', 'don', 'maynard'),
('(09) 321-1234', 'revis@island.com', 'darelle', 'revis');


INSERT INTO dogs VALUES
(1, 'Winning', '2014-02-01', 'Belgian Malinois', 'Aggressive', TRUE, 1, '123 Fake Street', 'South Coast'),
(2, 'Guaranteed', '2014-02-01', 'Belgian Malinois', 'Aggressive', TRUE, 1, '123 Fake Street', 'South Coast'),
(3, 'Eddy', '2008-09-08', 'Borzoi', 'Friendly', FALSE, 2, '142 Hill Street', 'Kelburn Hills'),
(4, 'Pimlico', '2010-12-11', 'Affenpinscher', 'Aggressive', FALSE, 2, '65 Top Street', 'Kelburn Hills'),
(5, 'Rex', '2017-06-05', 'Transylvanian Hound', 'Friendly', TRUE, 2, '123 City Street', 'Central Wellington'),
(6, 'Lux', '2015-05-28', 'English Springer Spaniel', 'Mild', TRUE, 3, '245 Wakefield Street', 'Central Wellington'),
(7, 'Precious', '2013-09-21', 'Chinese Crested', 'Mild', FALSE, 3, '99 Ranges Road', 'Miramar Peninsula'),
(8, 'Pete', '2015-06-19', 'Saint Bernard', 'Friendly', TRUE, 3, '1 Long Lane', 'Miramar Peninsula'),
(9, 'Hunter', '2013-04-21', 'Broholmer', 'Aggressive', TRUE, 3, '22 North Avenue', 'Northern Region'),
(10, 'Fido', '2016-12-25', 'Mountain Cur', 'Aggressive', TRUE, 4, '15 Lava Lane', 'Northern Region'),
(11, 'Rita', '2016-12-25', 'Mountain Cur', 'Friendly', FALSE, 5, '54 Wiltshire Terrace', 'Northern Region');


INSERT INTO statuses VALUES
('Booked'),
('Confirmed'),
('Cancelled'),
('Completed');


INSERT INTO bookings (fordog, length_minutes, walkdate, bookdate, status, contactdate) VALUES
(8, 75, '2020-01-05', '2019-12-26', 'Booked', '2019-12-28'),
(11, 75, '2020-01-07', '2019-12-31', 'Booked', '2020-01-01'),
(8, 60, '2020-01-16', '2020-01-04', 'Booked', '2020-01-05'),
(8, 60, '2020-01-12', '2020-01-05', 'Confirmed', '2020-01-05'),
(6, 105, '2020-01-16', '2020-01-11', 'Confirmed', '2020-01-12'),
(3, 105, '2020-01-25', '2020-01-17', 'Completed', '2020-01-19'),
(1, 75, '2020-01-21', '2020-01-17', 'Confirmed', '2020-01-17'),
(7, 90, '2020-01-26', '2020-01-20', 'Confirmed', '2020-01-21'),
(11, 75, '2020-02-01', '2020-01-21', 'Completed', '2020-01-23'),
(3, 120, '2020-01-30', '2020-01-22', 'Completed', '2020-01-23'),
(1, 90, '2020-02-01', '2020-01-27', 'Completed', '2020-01-29'),
(5, 30, '2020-02-03', '2020-01-28', 'Booked', '2020-01-29'),
(6, 120, '2020-02-04', '2020-01-30', 'Booked', '2020-01-30'),
(5, 15, '2020-02-05', '2020-01-30', 'Cancelled', '2020-01-31'),
(1, 90, '2020-02-06', '2020-01-31', 'Confirmed', '2020-02-02'),
(2, 105, '2020-02-12', '2020-02-01', 'Booked', '2020-02-01'),
(3, 120, '2020-02-12', '2020-02-04', 'Confirmed', '2020-02-06'),
(11, 120, '2020-02-18', '2020-02-08', 'Completed', '2020-02-10'),
(11, 60, '2020-02-24', '2020-02-13', 'Completed', '2020-02-15'),
(3, 120, '2020-02-25', '2020-02-13', 'Completed', '2020-02-14'),
(11, 15, '2020-03-04', '2020-02-22', 'Cancelled', '2020-02-23'),
(11, 15, '2020-03-04', '2020-02-23', 'Completed', '2020-02-25'),
(4, 105, '2020-03-13', '2020-03-01', 'Confirmed', '2020-03-03'),
(9, 75, '2020-03-14', '2020-03-04', 'Confirmed', '2020-03-06'),
(3, 75, '2020-03-14', '2020-03-04', 'Cancelled', '2020-03-05'),
(8, 45, '2020-03-09', '2020-03-05', 'Booked', '2020-03-05'),
(7, 90, '2020-03-16', '2020-03-07', 'Booked', '2020-03-07'),
(7, 120, '2020-03-15', '2020-03-09', 'Completed', '2020-03-10'),
(11, 60, '2020-03-27', '2020-03-15', 'Cancelled', '2020-03-17'),
(10, 105, '2020-03-26', '2020-03-20', 'Booked', '2020-03-20'),
(8, 45, '2020-04-03', '2020-03-26', 'Completed', '2020-03-28'),
(1, 45, '2020-04-07', '2020-04-01', 'Confirmed', '2020-04-01'),
(5, 30, '2020-04-11', '2020-04-03', 'Booked', '2020-04-03'),
(8, 45, '2020-04-09', '2020-04-04', 'Booked', '2020-04-06'),
(10, 90, '2020-04-15', '2020-04-05', 'Completed', '2020-04-05'),
(11, 105, '2020-04-16', '2020-04-07', 'Confirmed', '2020-04-08'),
(6, 60, '2020-04-14', '2020-04-07', 'Booked', '2020-04-08'),
(10, 15, '2020-04-19', '2020-04-13', 'Confirmed', '2020-04-14'),
(11, 105, '2020-04-23', '2020-04-13', 'Confirmed', '2020-04-14'),
(5, 15, '2020-04-23', '2020-04-13', 'Cancelled', '2020-04-13'),
(10, 60, '2020-04-26', '2020-04-14', 'Confirmed', '2020-04-14'),
(10, 30, '2020-04-19', '2020-04-15', 'Booked', '2020-04-17'),
(7, 75, '2020-04-22', '2020-04-17', 'Cancelled', '2020-04-17'),
(5, 30, '2020-04-23', '2020-04-17', 'Confirmed', '2020-04-19'),
(6, 90, '2020-04-26', '2020-04-18', 'Cancelled', '2020-04-19'),
(1, 75, '2020-05-10', '2020-05-02', 'Booked', '2020-05-03'),
(11, 60, '2020-05-14', '2020-05-03', 'Cancelled', '2020-05-03'),
(5, 60, '2020-05-09', '2020-05-03', 'Confirmed', '2020-05-04'),
(3, 45, '2020-05-09', '2020-05-05', 'Booked', '2020-05-06'),
(5, 60, '2020-05-15', '2020-05-09', 'Confirmed', '2020-05-09'),
(10, 30, '2020-05-20', '2020-05-15', 'Confirmed', '2020-05-17'),
(4, 30, '2020-05-20', '2020-05-12', 'Booked', '2020-05-12'),
(5, 75, '2020-05-21', '2020-05-17', 'Confirmed', '2020-05-18'),
(3, 120, '2020-05-22', '2020-05-13', 'Cancelled', '2020-05-13'),
(5, 90, '2020-05-23', '2020-05-15', 'Cancelled', '2020-05-17'),
(11, 90, '2020-05-25', '2020-05-16', 'Confirmed', '2020-05-17'),
(9, 105, '2020-05-28', '2020-05-21', 'Confirmed', '2020-05-23'),
(10, 75, '2020-05-29', '2020-05-25', 'Cancelled', '2020-05-25'),
(3, 75, '2020-05-30', '2020-05-26', 'Confirmed', '2020-05-27'),
(10, 105, '2020-05-31', '2020-05-22', 'Booked', '2020-05-25'),
(10, 120, '2020-05-31', '2020-05-21', 'Booked', '2020-05-21'),
(6, 45, '2020-06-01', '2020-05-21', 'Cancelled', '2020-05-22'),
(2, 15, '2020-06-02', '2020-05-22', 'Confirmed', '2020-05-22'),
(2, 75, '2020-06-03', '2020-05-25', 'Cancelled', '2020-05-26'),
(3, 75, '2020-06-04', '2020-05-25', 'Cancelled', '2020-05-28'),
(3, 15, '2020-06-05', '2020-05-29', 'Confirmed', '2020-05-31'),
(11, 75, '2020-06-05', '2020-06-01', 'Booked', '2020-06-01'),
(11, 30, '2020-06-06', '2020-06-02', 'Confirmed', '2020-06-04'),
(7, 30, '2020-06-07', '2020-06-03', 'Booked', '2020-06-06'),
(9, 45, '2020-06-09', '2020-06-03', 'Confirmed', '2020-06-03'),
(8, 45, '2020-06-09', '2020-06-02', 'Confirmed', '2020-06-04'),
(1, 15, '2020-06-10', '2020-05-30', 'Confirmed', '2020-06-02'),
(11, 90, '2020-06-12', '2020-06-08', 'Booked', '2020-06-08'),
(10, 90, '2020-06-15', '2020-06-05', 'Cancelled', '2020-06-06'),
(2, 105, '2020-06-16', '2020-06-12', 'Confirmed', '2020-06-13'),
(8, 90, '2020-06-17', '2020-06-11', 'Confirmed', '2020-06-13'),
(6, 45, '2020-06-17', '2020-06-09', 'Cancelled', '2020-06-12'),
(10, 75, '2020-06-20', '2020-06-13', 'Confirmed', '2020-06-13'),
(11, 15, '2020-06-23', '2020-06-14', 'Cancelled', '2020-06-14'),
(1, 90, '2020-06-23', '2020-06-13', 'Booked', '2020-06-15'),
(11, 60, '2020-06-23', '2020-06-16', 'Booked', '2020-06-17'),
(10, 15, '2020-06-24', '2020-06-15', 'Confirmed', '2020-06-16'),
(11, 30, '2020-06-25', '2020-06-19', 'Cancelled', '2020-06-19'),
(3, 30, '2020-06-25', '2020-06-16', 'Confirmed', '2020-06-18'),
(3, 105, '2020-06-27', '2020-06-18', 'Cancelled', '2020-06-21'),
(11, 120, '2020-06-27', '2020-06-16', 'Confirmed', '2020-06-18'),
(6, 15, '2020-06-28', '2020-06-22', 'Confirmed', '2020-06-22'),
(5, 105, '2020-06-28', '2020-06-24', 'Cancelled', '2020-06-24'),
(8, 120, '2020-06-29', '2020-06-24', 'Confirmed', '2020-06-24'),
(3, 15, '2020-07-02', '2020-06-21', 'Confirmed', '2020-06-23'),
(5, 120, '2020-07-03', '2020-06-29', 'Booked', '2020-06-29'),
(8, 30, '2020-07-03', '2020-06-22', 'Confirmed', '2020-06-25'),
(4, 60, '2020-07-03', '2020-06-23', 'Booked', '2020-06-23'),
(4, 60, '2020-07-05', '2020-06-26', 'Confirmed', '2020-06-29'),
(10, 105, '2020-07-05', '2020-06-30', 'Confirmed', '2020-06-30'),
(6, 75, '2020-07-07', '2020-06-27', 'Cancelled', '2020-06-27'),
(4, 75, '2020-07-08', '2020-06-28', 'Cancelled', '2020-06-29'),
(5, 120, '2020-07-10', '2020-07-05', 'Booked', '2020-07-07'),
(5, 45, '2020-07-11', '2020-07-01', 'Booked', '2020-07-04'),
(4, 90, '2020-07-14', '2020-07-07', 'Confirmed', '2020-07-10');


INSERT INTO walks VALUES
(1, 1),
(2, 5),
(3, 11),
(4, 1),
(5, 2),
(6, 1),
(7, 4),
(8, 8),
(9, 4),
(10, 2),
(11, 4),
(12, 9),
(13, 5),
(14, 9),
(15, 13),
(16, 12),
(17, 4),
(18, 3),
(19, 5),
(20, 11),
(21, 2),
(22, 3),
(23, 7),
(24, 1),
(25, 11),
(26, 13),
(27, 12),
(28, 1),
(29, 3),
(30, 2),
(31, 1),
(32, 2),
(33, 9),
(34, 11),
(35, 4),
(36, 4),
(37, 13),
(38, 4),
(39, 3),
(40, 1),
(41, 5),
(42, 3),
(43, 8),
(44, 5),
(45, 11),
(46, 13),
(47, 4),
(48, 1),
(49, 2),
(50, 2),
(51, 1),
(52, 5),
(53, 11),
(54, 1),
(55, 2),
(56, 1),
(57, 4),
(58, 8),
(59, 4),
(60, 2),
(61, 4),
(62, 9),
(63, 5),
(64, 9),
(65, 13),
(66, 12),
(67, 4),
(68, 3),
(69, 5),
(70, 11),
(71, 2),
(72, 3),
(73, 7),
(74, 1),
(75, 11),
(76, 13),
(77, 12),
(78, 1),
(79, 3),
(80, 2),
(81, 1),
(82, 2),
(83, 9),
(84, 11),
(85, 4),
(86, 4),
(87, 13),
(88, 4),
(89, 3),
(90, 1),
(91, 5),
(92, 3),
(93, 8),
(94, 5),
(95, 11),
(96, 13),
(97, 4),
(98, 1),
(99, 2),
(100, 2);
