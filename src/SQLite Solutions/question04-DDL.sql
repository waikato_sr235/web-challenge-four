DROP TABLE IF EXISTS walks;
DROP TABLE IF EXISTS bookings;
DROP TABLE IF EXISTS statuses;
DROP TABLE IF EXISTS dogs;
DROP TABLE IF EXISTS owners;
DROP TABLE IF EXISTS walkers;
DROP TABLE IF EXISTS suburbs;
DROP TABLE IF EXISTS regions;
DROP TABLE IF EXISTS breeds;
DROP TABLE IF EXISTS sizes;

-- id not technically needed, but makes for nice logic to validate whether a dog is appropriately sized for its walker
CREATE TABLE IF NOT EXISTS sizes (
    id INTEGER PRIMARY KEY,
    name VARCHAR(10)
);

CREATE TABLE IF NOT EXISTS breeds (
    name VARCHAR(25) PRIMARY KEY,
    size INTEGER NOT NULL,
    FOREIGN KEY (size) REFERENCES sizes(id)
);

CREATE TABLE IF NOT EXISTS regions (
    name VARCHAR(20) PRIMARY KEY
);


CREATE TABLE IF NOT EXISTS suburbs (
    postcode VARCHAR(4),
    name VARCHAR(20),
    region VARCHAR(20) NOT null,
    PRIMARY KEY (postcode, name), -- Composite primary key needed as there isn't a 1:1 relationship between suburbs and postcodes
    FOREIGN KEY (region) REFERENCES regions(name)
);

CREATE TABLE IF NOT EXISTS walkers (
    ir_num INTEGER PRIMARY KEY,
    fname VARCHAR(20),
    lname VARCHAR(40) NOT NULL,
    region VARCHAR(20) NOT NULL,
    daily_time_minutes INTEGER NOT NULL,
    max_size INTEGER NOT NULL,
    FOREIGN KEY (region) REFERENCES regions(name),
    FOREIGN KEY (max_size) REFERENCES sizes(id)
);

CREATE TABLE IF NOT EXISTS owners (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    phone VARCHAR(13),
    email VARCHAR(100),
    fname VARCHAR(20),
    lname VARCHAR(40),
    CONSTRAINT phone_number CHECK (
           phone GLOB '(0[3-9]) [0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]'   -- valid NZ area codes are 03 or greater
        OR phone GLOB '02[0-9] [0-9][0-9][0-9] [0-9][0-9][0-9]'
        OR phone GLOB '02[0-9] [0-9][0-9][0-9] [0-9][0-9][0-9][0-9]'
        OR phone GLOB '02[0-9] [0-9][0-9][0-9] [0-9][0-9][0-9][0-9][0-9]')       -- all NZ cell phone numbers start 02
);

CREATE TABLE IF NOT EXISTS dogs (
    registration INT PRIMARY KEY,
    name VARCHAR(40),
    dob DATE,
    breed VARCHAR(25),
    temperament VARCHAR(20),
    equipped BOOL,
    owner INTEGER,
    streetAddress VARCHAR(40),
    region VARCHAR(20),
    FOREIGN KEY (breed) REFERENCES breeds(name),
    FOREIGN KEY (owner) REFERENCES owners(id) ON DELETE CASCADE,
    FOREIGN KEY (region) REFERENCES regions(name)
);

CREATE TABLE IF NOT EXISTS statuses (
    name VARCHAR(20) PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS bookings (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    forDog INTEGER,
    length_minutes INTEGER NOT NULL,
    walkDate DATE NOT NULL,
    bookDate DATE NOT NULL,
    status VARCHAR(20) NOT NULL,
    contactDate DATE,
    FOREIGN KEY (forDog) REFERENCES dogs(registration) ON DELETE SET NULL,
    FOREIGN KEY (status) REFERENCES statuses(name),
    CHECK (julianday(walkDate) - julianday(bookDate) >= 3)
);

CREATE TABLE IF NOT EXISTS walks (
    forBooking INTEGER PRIMARY KEY,
    walker INTEGER,
    FOREIGN KEY (walker) REFERENCES walkers(ir_num) ON DELETE SET NULL
);

