-- How many dogs need to be walked in the next 7 days (from the current date, inclusive) in each region.
SELECT d.region as Region, COUNT(b.id) as Count FROM
        bookings b
JOIN
    dogs d ON (b.forDog = d.registration)
WHERE
    status != 'Cancelled' AND
    julianday(walkdate) >= julianday(date('now')) AND
    julianday(walkDate) - julianday(date('now')) < 7
GROUP BY
    region;

-- What is the name and contact phone number for owners that have requested a
-- walking session in a region where there are no walkers? Further, what is the name of
-- the pet they have booked for?

SELECT
    (o.fname || ' ' || o.lname) Name,
    o.phone 'Phone Number',
    d.name 'Dog Name'
FROM
    bookings b
        JOIN
    dogs d ON b.forDog = d.registration
        JOIN
    owners o ON d.owner = o.id
WHERE d.region NOT IN (
    SELECT w.region FROM
        walkers w
    GROUP BY
        w.region)
GROUP BY
    d.name;

-- The name, breed and age in days of each dog in the database. Output should be
-- ordered from smallest dog breeds to largest, then by the breed name alphabetically A
-- - Z, and finally age oldest to youngest.

SELECT d.name Name, d.breed Breed, (julianday(date('now')) - julianday(dob)) Age FROM
    dogs d
JOIN
    breeds b on d.breed = b.name
JOIN
    sizes s on b.size = s.id
ORDER BY
    s.id, breed, age; -- s.id is in order from smallest to largest so will sort appropriately

-- How many minutes each dog walker spent walking dogs each day.
SELECT
       walkDate Date,
       (e.fname || ' ' || e.lname) Walker,
       SUM(ALL b.length_minutes) Amount
FROM
    walks w
JOIN
    bookings b ON b.id = w.forBooking
JOIN
    walkers e on w.walker = e.ir_num
WHERE
    b.status != 'Cancelled'
GROUP BY
    e.ir_num, b.walkDate;

-- Which walkers have exceeded their maximum number of walking time in a day, and
-- how many occasions has this happened for each. Output should be ordered by the
-- number of occasions descending.

SELECT (w.fname || ' ' || w.lname) Walker, COUNT(ir_num) Occasions FROM
    walkers w
JOIN
    (SELECT
        walkDate Date,
        e.ir_num walkerId,
        SUM(ALL b.length_minutes) Amount
    FROM
        walks w
            JOIN
        bookings b ON b.id = w.forBooking
            JOIN
        walkers e on w.walker = e.ir_num
    WHERE
            b.status != 'Cancelled'
    GROUP BY
        e.ir_num, b.walkDate) e
ON
    w.ir_num = e.walkerId
WHERE
    e.Amount > w.daily_time_minutes
GROUP BY
    Walker;

-- Create a new view called overworked_walkers that is defined by your solution to part e).

CREATE VIEW overworked_walkers AS
SELECT (w.fname || ' ' || w.lname) Walker, COUNT(ir_num) Occasions FROM
    walkers w
        JOIN
    (SELECT
         walkDate Date,
         e.ir_num walkerId,
         SUM(ALL b.length_minutes) Amount
     FROM
         walks w
             JOIN
         bookings b ON b.id = w.forBooking
             JOIN
         walkers e on w.walker = e.ir_num
     WHERE
             b.status != 'Cancelled'
     GROUP BY
         e.ir_num, b.walkDate) e
    ON
            w.ir_num = e.walkerId
WHERE
        e.Amount > w.daily_time_minutes
GROUP BY
    Walker;

-- Use this new view to show only walkers who have between 1 and 5 days where they have exceeded their maximum walking time per day.

SELECT * FROM
    overworked_walkers
WHERE
    Occasions >= 1 AND Occasions <= 5;